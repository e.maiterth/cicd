export interface TaskItem {
  title: string;
  description: string;
  dueDate: Date;
  done: boolean;
}
