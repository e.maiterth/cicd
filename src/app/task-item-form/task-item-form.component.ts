import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  ReactiveFormsModule,
  ValidationErrors,
  ValidatorFn,
  Validators
} from '@angular/forms';
import {TaskItemService} from '../task-item.service';
import {TaskItem} from '../task-item';
import {FormControlRowComponent} from "../form-control-row/form-control-row.component";
import {formatDate, NgClass} from "@angular/common";

@Component({
  selector: 'app-task-item-form',
  standalone: true,
  imports: [ReactiveFormsModule, FormControlRowComponent, NgClass],
  templateUrl: './task-item-form.component.html',
  styleUrl: './task-item-form.component.scss'
})
export class TaskItemFormComponent implements OnInit {

  @Input() taskItem: TaskItem | undefined;

  @Output() formFinished = new EventEmitter();

  formGroup!: FormGroup;

  constructor(private readonly fb: FormBuilder,
              private readonly taskItemsService: TaskItemService) {}

  ngOnInit(): void {
    this.formGroup = this.fb.group({
      title: [this.taskItem ? this.taskItem.title : null, Validators.required],
      description: [this.taskItem ? this.taskItem.description : null, Validators.required],
      // etwas komplexer mit formatDate, weil input type="date" einen String erwartet
      dueDate: [this.taskItem ? formatDate(this.taskItem.dueDate, 'yyyy-MM-dd', 'de') : null, [Validators.required, futureValidator()]]
    });
  }

  cancelEdit(): void {
    this.formFinished.emit();
  }

  saveTaskItem(): void {
    if (this.formGroup) {
      this.formGroup.markAllAsTouched();
    }
    if (this.formGroup?.valid) {
      const newTaskItem: TaskItem = {
        title: this.formGroup.value.title,
        description: this.formGroup.value.description,
        dueDate: new Date(this.formGroup.value.dueDate),
        done: false
      };
      if (this.taskItem) {
        this.taskItem.title = newTaskItem.title;
        this.taskItem.description = newTaskItem.description;
        this.taskItem.dueDate = newTaskItem.dueDate;
        this.formFinished.emit();
        if (this.taskItemsService) {
          this.taskItemsService.persistTaskItems();
        }
      } else if (this.taskItemsService) {
          this.taskItemsService.addTaskItem(newTaskItem);
          this.formGroup.reset();
      }
    }
  }
}

function futureValidator(): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (!control.value) {
      return null;
    }
    const now = new Date();
    const dateValue = new Date(control.value);
    const invalid = now.getTime() > dateValue.getTime();
    const dateString = formatDate(now, 'dd.MM.yyyy', 'de')
    return invalid ? {future: {errorMessage: `Bitte geben Sie ein Datum nach dem ${dateString} an`}} : null;
  };
}

