import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormControlRowComponent } from './form-control-row.component';

describe('FormControlRowComponent', () => {
  let component: FormControlRowComponent;
  let fixture: ComponentFixture<FormControlRowComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [FormControlRowComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(FormControlRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
