import {Component, Input} from '@angular/core';
import {AbstractControl} from '@angular/forms';
import {NgClass} from '@angular/common';

@Component({
  selector: 'app-form-control-row',
  standalone: true,
  imports: [NgClass],
  templateUrl: './form-control-row.component.html',
  styleUrl: './form-control-row.component.scss'
})
export class FormControlRowComponent {

  @Input() label = '';

  @Input() for: string | undefined;

  @Input() control: AbstractControl | undefined;

  get showError(): boolean {
    if (this.control) {
      return this.control.invalid && (this.control.touched || this.control.dirty || this.control.value);
    }
    return false
  }

  get errorMessage(): string {
    if (this.control?.errors) {
      if (this.control.errors['required'] === true) {
        return 'Bitte geben Sie einen Wert an';
      }
      else if (this.control.errors['future']) {
        return this.control.errors['future']['errorMessage'];
      }
    }
    return '';
  }

}
